shell.executable("/bin/bash")

import os

GIT_PATH = "/home/user/Downloads/stool-preservation/"


KRAKEN_DB = GIT_PATH + "Kraken_RefSeqTL/"
DATA_PATH = GIT_PATH + "data/"
SAMPLE_SHEET = GIT_PATH + "data/SAMPLE_SHEET.csv"

FASTQs = expand(DATA_PATH + "FASTQ/" + "{fq_files}", fq_files = [l.split(',')[1].split("/")[-1] for l in open(SAMPLE_SHEET, "r").readlines()[1:]])

rule target :
    input :
        DATA_PATH+"OTU-table.tsv",
        DATA_PATH+"rep-seqs.fasta",
        DATA_PATH+"taxonomy.tsv"


##################################################
###       Download data from this study        ###
##################################################

rule download_data :
    input :
        SAMPLE_SHEET
    output :
        FASTQs
    params :
        FASTQ_PATH = DATA_PATH + "FASTQ/"
    threads :
        1
    shell :
        "sed -n '1d;p' {input} | cut -d ',' -f4 | wget -P {params.FASTQ_PATH} -i - "
        

##################################################
###          ANALYSIS PIPELINE                 ###
##################################################


rule q2_import :
    input :
        SAMPLE_SHEET = SAMPLE_SHEET,
        FASTQs = FASTQs
    output :
        SAMPLE_SHEET_clear = temp(DATA_PATH+"SAMPLE_SHEET.clear.csv"),
        qza = temp(DATA_PATH+"demux-paired-end.qza")
    threads :
        1
    conda :
        "conda_envs/qiime2-2019.10.yml"
    shell :
        "cut -d ',' -f1,2,3 {input.SAMPLE_SHEET}>{output.SAMPLE_SHEET_clear};"
        "qiime tools import "
        "--type 'SampleData[PairedEndSequencesWithQuality]' "
        "--input-path {output.SAMPLE_SHEET_clear} " 
        "--input-format PairedEndFastqManifestPhred33 "
        "--output-path {output.qza} "

rule q2_denoise :
    input :
        DATA_PATH+"demux-paired-end.qza"
    output :
        tab = temp(DATA_PATH+"table-dada2.qza") ,
        seq = temp(DATA_PATH+"rep-seqs-dada2.qza") ,
        stat = temp(DATA_PATH+"denoising-stats.qza")
    threads :
        8
    conda :
        "conda_envs/qiime2-2019.10.yml"
    shell :
        "qiime dada2 denoise-paired "
        "--i-demultiplexed-seqs {input} "
        "--p-trunc-len-f 245 "
        "--p-trunc-len-r 245 "
        "--p-trim-left-f 17 "
        "--p-trim-left-r 21 "
        "--p-n-threads {threads} "
        "--o-table {output.tab} "
        "--o-representative-sequences {output.seq} "
        "--o-denoising-stats {output.stat} "

rule q2_export :
    input :
        tab = DATA_PATH+"table-dada2.qza",
        seq = DATA_PATH+"rep-seqs-dada2.qza" 
    output :
        tab = protected(DATA_PATH+"OTU-table.tsv"),
        seq = protected(DATA_PATH+"rep-seqs.fasta")
    params :
        DATA_PATH = DATA_PATH
    threads :
        1
    conda :
        "conda_envs/qiime2-2019.10.yml"
    shell :
        "qiime tools export --input-path {input.seq} --output-path {params.DATA_PATH};"
        "mv {params.DATA_PATH}/dna-sequences.fasta {output.seq};"
        "qiime tools export --input-path {input.tab} --output-path {params.DATA_PATH};"
        "biom convert --to-tsv --input-fp {params.DATA_PATH}/feature-table.biom --output-fp {output.tab}"



rule kraken :
    input :
        DATA_PATH+"rep-seqs.fasta"
    output :
        protected(DATA_PATH+"taxonomy.tsv")
    params :
        KRAKEN_DB = KRAKEN_DB
    threads :
        8
    conda :
        "conda_envs/kraken-1.1.yml"
    shell :
        "kraken {input} --threads {threads} --db {params.KRAKEN_DB} | kraken-translate --db {params.KRAKEN_DB} --mpa-format > {output}"






