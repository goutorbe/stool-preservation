Git repo containing scripts related to *Human stool preservation impacts taxonomic profiles in 16S metagenomics studies*, Plauzolles et al 2020.

# Requirements

Bioinformatics steps needs to be performed under Linux OS, with RAM memory >16Gb and hard disk space >30Gb.  
Conda package management system is needed. Instructions for installing conda can be found [here](https://docs.conda.io).  
**NOTE :** statistical analysis can be run independantly, as OTU table, representative sequences and their taxonomical assignation are available in `data/`.  

# Set up

First you need to clone this git repo :  
`git clone https://gitcrcm.marseille.inserm.fr/goutorbe/stool-preservation.git`  
`cd stool-preservation`  

You need to uncompress the pre-compiled Kraken DataBase for Refeseq Targted Loci Project :  
`tar zxvf Kraken_RefSeqTL.tar.gz`  

You need to edit the file called `Snakefile`, line 5 : change the `GIT_PATH` to the actual path where you cloned the GIT repo.

# Bioinformatic pipeline

All steps are wrapped into *Snakemake* rules, executed in *conda* environments.  
You can execute the pipeline by executing the command :  
`chmod 711 RunSnake.sh ; ./RunSnake.sh`  

# Statistical analysis

All R code used for this analysis is available in `statistical-analysis.Rmd`.  
It uses Rmarkdown format, you can execute it by running the following command `Rscript -e "rmarkdown::render(\"statistical-analysis.Rmd\")"`.  
The standalone HTML file `statistical-analysis.html` contains both code and its ouputs (figures and statistics).
