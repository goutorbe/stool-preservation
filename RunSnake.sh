#!/bin/bash

source $CONDA_PREFIX/etc/profile.d/conda.sh
conda env create -f conda_envs/snakemake-5.10.0.yml
conda activate snakemake-5.10.0



snakemake --snakefile Snakefile --rulegraph | dot -Tpng > dag.png

snakemake \
--snakefile Snakefile \
--use-conda \
--jobs 1 \
--keep-going \
--latency-wait 150 \
--verbose \
--printshellcmds #\
#--dryrun \
#--create-envs-only


